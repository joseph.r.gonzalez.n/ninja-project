﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombatv2 : MonoBehaviour
{
    //animator for the control of the animations
    public Animator animator;

    //position for the attack point
    public Transform attackPoint;
    private Vector3 normalAttackPointPosition;

    //detect only object that are in this layer
    [SerializeField] private LayerMask destroyObjectLayer;

    //attack behaviour
    public float attackRange = 0.5f;
    public int attackDamage = 40;
    public float attackRate = 2f;
    float nextAttackTime = 0f;

    //Control Input Variables
    private bool atackPressed = false;

    void Start()
    {
        normalAttackPointPosition = attackPoint.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Attack"))
        {
            atackPressed = true;
        }
        if (Input.GetButtonUp("Attack"))
        {
            atackPressed = false;
        }

        //Controlling the attack rate
        if (Time.time >= nextAttackTime)
        {

            //Controlling the three types of attack
            if (atackPressed && Input.GetAxisRaw("Vertical") > 0)
            {
                Debug.Log("Up Attack");
                UpAttack();
                nextAttackTime = Time.time + 1 / attackRate;
            }
            else if (animator.GetBool("IsJumping") && Input.GetAxisRaw("Vertical") < 0 && atackPressed)
            {
                Debug.Log("Down Attack");
                DownAttack();
                nextAttackTime = Time.time + 1 / attackRate;
            }
            else if (Input.GetButtonDown("Attack"))
            {
                Debug.Log("Left-Right Attack");
                
                LeftRightAttack();
                nextAttackTime = Time.time + 1 / attackRate;

            }
            //Controlling the readjustment of the AttackPoint
            else if (GetComponent<CharacterController2D>().m_FacingRight)
            {
                attackPoint.position = transform.position + new Vector3(1f, 0.0f);
            }
            else if (!GetComponent<CharacterController2D>().m_FacingRight)
            {
                attackPoint.position = transform.position + new Vector3(-1f, 0.0f); ;
            }
        }

    }

    void UpAttack()
    {
        //Play attack animation


        //Changing the attack Point
        attackPoint.position = transform.position + new Vector3(0.0f, 1f);

        //Control  the collision with enemies
        EnemyTakeDamage();
    }

    void DownAttack()
    {
        //Play attack animation
        animator.SetBool("IsJumping", false);
        animator.SetTrigger("IsRollingAttacking");

        //Changing the attack Point
        attackPoint.position = transform.position + new Vector3(0.0f, -1f);

        //Control the enemies taken damage
        EnemyTakeDamage();
    }

    void LeftRightAttack()
    {
        //Play attack animation
        animator.SetTrigger("IsAttacking");

        //Control the enemies taken damage
        EnemyTakeDamage();
    }

    void EnemyTakeDamage()
    {
        //Detect enemies in range of attack
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, destroyObjectLayer);

        // Damage the enemies
        foreach (Collider2D enemy in hitEnemies)
        {
            if (enemy.tag == "Enemy1")
            {
                enemy.GetComponent<Enemy>().TakeDamage(attackDamage);
            }
            else if (enemy.tag == "Enemy2")
            {
                enemy.GetComponent<EnemyRange>().TakeDamage(attackDamage);
            }
            else if (enemy.tag == "Item")
            {
                enemy.GetComponent<ItemStats>().TakeDamage(attackDamage);
                GetComponent<AttackEffects>().CreateHitEffect(attackPoint.position);
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
            return;
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }


}
