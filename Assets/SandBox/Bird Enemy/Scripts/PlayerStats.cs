﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class PlayerStats : CharacterStats
{

    //References
    Rigidbody2D rbPlayer;
    PlayerMovement movement;
    PlayerAttack attack;
    CinemachineImpulseSource shakeCamera;
    //If you want items that modified this stat create with Stat class
    //public Stat speed;
    //public Stat attackRate;
    //public Stat attackRange;

    [Header("Attack Stats")]
    public float attackRate = 2f; 
    public float attackRange = 0.5f;
    public float horizontalforce = 5;

    [Header("Movement Stats")]
    public float speed = 12f;
    public float bounceforce = 10;

    [Header("Player State")]
    int hitParamID;
    public bool takeHit;
    public float TimeforHit = 1f;

    Animator animator;
    public GameObject VFXdamage;
    PlayerAttackInput input;

    // Start is called before the first frame update
    void Start()
    {
        rbPlayer = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        hitParamID = Animator.StringToHash("isHit");
        input = GetComponent<PlayerAttackInput>();
        movement = GetComponent<PlayerMovement>();
        attack = GetComponent<PlayerAttack>();
        shakeCamera = GetComponent<CinemachineImpulseSource>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    public override void TakeDamage(int damage)
    {
        if (input.defenseHeld)
            return;

        
        animator.SetTrigger(hitParamID);
        BeginDamage();
        base.TakeDamage(damage);
        
    }

    public void BeginDamage()
    {
        takeHit = true;
        ClearPreviousState();
        Time.timeScale = 0.7f;
        //shakeCamera.GenerateImpulse();
        rbPlayer.velocity = Vector3.zero;
        float forceValue = 40f;
        rbPlayer.AddForce(Vector2.right * forceValue * -movement.direction, ForceMode2D.Impulse);
        Debug.Log("Hit");
        Invoke("EndDamage", TimeforHit);
    }

    void EndDamage()
    {
        Time.timeScale = 1f;
        takeHit = false;

    }



    void ClearPreviousState()
    {
        attack.attacking = false;
        movement.isHanging = false;
        movement.canWallJump = false;
        movement.isJumping = false;
    }



    public override void  InstantianteEffect()
    {
        
        Instantiate(VFXdamage, transform.position, transform.rotation);
    }
}
