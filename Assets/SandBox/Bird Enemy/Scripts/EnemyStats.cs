﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class EnemyStats : CharacterStats
{


    [Header("Attack Stats")]
    public float attackRate = 2f;
    public float attackRange = 0.5f;
    public int attackDamage = 10;
    public GameObject VFXeffect;

    [Header("Movement Stats")]
    public float speed;

    //to neglect left right attack
    EnemyCover coverandJump;


    void Start()
    {
        //cover
        coverandJump = GetComponent<EnemyCover>();
    }

    public override void TakeDamage(int damage)
    {
        //if raycast is true then no damage to enemy and execute specific method for effects and animation
        if (coverandJump.detectplayer)
        {
            coverandJump.CoverAttack();

        }
        else
        {
            base.TakeDamage(damage);
        }
    }

    public override void Die()
    {
        Destroy(gameObject);
        //effect
        //sound
        //animation
    }

    public override void InstantianteEffect()
    {
        Instantiate(VFXeffect, transform.position, transform.rotation);
    }

}
