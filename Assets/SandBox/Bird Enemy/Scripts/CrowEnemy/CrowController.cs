﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Pathfinding;

public class CrowController : MonoBehaviour
{
    [Header("Movement and Detection")]
    public Transform[] patrolTargets;
    public Transform crowArea;
    public Vector2 detectionPoint;
    public float detectionRange = 5f;
    public bool isPlayerDetecting = false;

    [Header("Attack Stats")]
    public float attackRange = 0.5f;

    [Header("Attack Properties")]
    public Vector2 attackPoint;
    public float patrolTime = 5f;
    public float followTime = 5f;
    public float attackTime = 5f;
    public float attackOffset = 1f;
    [SerializeField] private LayerMask playerLayer;

    [Header("Position Properties")]
    public bool crowStartInAir = false;

    [Header("Movement Stats")]
    public float maxSpeed = 3;

    private int index;

    IAstarAI agent;
    CrowStats stats;
    Transform player;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        agent = GetComponent<IAstarAI>();
        stats = GetComponent<CrowStats>();
    }

    private void Update()
    {
        agent.maxSpeed = maxSpeed; //Change this maxSpeed in the awake method once your define a correct speed
    }

    public void FollowToPlayer()
    {
        //agent.destination = playerTarget.GetChild(2).position;
        agent.destination = (Vector2) player.position + new Vector2(0f, attackOffset);


        if (!agent.pathPending)
        {
            agent.SearchPath();
        }
    }

    public void Patrol()
    {

        if (patrolTargets.Length == 0) return;

        bool search = false;

        // Check if the agent has reached the current target.
        // We must check for 'pathPending' because otherwise we might
        // detect that the agent has reached the *previous* target
        // because the new path has not been calculated yet.
        if (agent.reachedEndOfPath && !agent.pathPending)
        {
            index = index + 1;
            search = true;
        }

        // Wrap around to the start of the targets array if we have reached the end of it
        index = index % patrolTargets.Length;
        agent.destination = patrolTargets[index].position;

        // Immediately calculate a path to the target.
        // Note that this needs to be done after setting the destination.
        if (search) agent.SearchPath();
    }

    public void DetectPlayer()
    {
        Collider2D[] player = Physics2D.OverlapCircleAll((Vector2) crowArea.position + detectionPoint, detectionRange, playerLayer);

        if(player.Length == 1)
        {
            if (player[0].tag == "Player")
            {
                isPlayerDetecting = true;
            }
        }
        else{
            isPlayerDetecting = false ;
        }
    }
    public void DamagePlayer()
    {
        //Detect enemies in range of attack
        Collider2D player = Physics2D.OverlapCircle(transform.position, attackRange, playerLayer);

        // Damage the enemies
        if (player != null)
        {
            player.GetComponent<PlayerStats>().TakeDamage(stats.damage.GetValue());
        }
    }

    public float DistanceCrowPlayer()
    {
        return Vector2.Distance((Vector2)player.position + new Vector2(0f, attackOffset), (Vector2)transform.position + attackPoint);
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere((Vector2) transform.position + attackPoint, attackRange);

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere((Vector2)crowArea.position + detectionPoint, detectionRange);

        if (player != null)
            Gizmos.DrawIcon((Vector2)player.position + new Vector2(0f, attackOffset), "Light Gizmo.tiff", true);
    }

}
