﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrowIdle : StateMachineBehaviour
{
    CrowController controller;
    float patrolTime;

    //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        controller = animator.GetComponentInParent<CrowController>();
        patrolTime = Time.time + controller.patrolTime;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        controller.DetectPlayer();

        if (Time.time >= patrolTime && controller.isPlayerDetecting)
        {
            animator.SetBool("IsFollowing", true);
        }
        else
        {
            controller.Patrol();
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

}
