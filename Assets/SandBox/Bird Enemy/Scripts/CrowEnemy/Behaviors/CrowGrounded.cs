﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrowGrounded : StateMachineBehaviour
{
    CrowController controller;

    //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        controller = animator.GetComponentInParent<CrowController>();

        if (controller.crowStartInAir)
        {
            animator.SetBool("OnGround", false);
        }
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!controller.crowStartInAir)
        {
            controller.DetectPlayer();
            if (controller.isPlayerDetecting)
            {
                animator.SetBool("OnGround", false);
            }
        }

    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
    }

}
