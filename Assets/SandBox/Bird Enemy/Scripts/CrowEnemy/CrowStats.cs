﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrowStats : CharacterStats
{
    [Header("Hurt Assets")]
    public GameObject hurtEffect;

    //[Header("Died Assets")]


    public override void Die()
    {
        Destroy(gameObject);
        //effect
        //sound
        //animation
    }

    public override void TakeDamage(int damage)
    {
        base.TakeDamage(damage);

        if (hurtEffect != null) Instantiate(hurtEffect, transform.position, transform.rotation);

        //sound
        //animation
    }


}
