﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrowGFX : MonoBehaviour
{

    int direction = -1;
    AIPath agent;
    CrowController controller;


    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponentInParent<AIPath>();
        controller = GetComponentInParent<CrowController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (agent.desiredVelocity.x * direction < 0)
        {
            Flip();
        }
    }

    public void DamagePlayer()
    {
        controller.DamagePlayer();
    }

    public void Flip()
    {
        direction *= -1;
        transform.Rotate(0f, 180f, 0f);
    }
}
