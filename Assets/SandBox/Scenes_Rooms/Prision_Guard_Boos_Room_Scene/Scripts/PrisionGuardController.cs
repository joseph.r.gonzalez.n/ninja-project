﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrisionGuardController : MonoBehaviour
{
    public float normalSpeed = 10f;
    [Range(0,1)]
    public float smoothingSpeedFactor = 0.6f; 

    public float detectForce = 10f;

    int direction = -1;
    bool isFlipped = false;

    Transform player;
    Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        LookAtPlayer();
    }

    public void FollowPlayer()
    {

        Vector2 current_velocity = rb.velocity;
        Vector2 tvel = direction * new Vector2(normalSpeed, 0);
        rb.velocity = Vector2.Lerp(current_velocity, tvel, Time.deltaTime * smoothingSpeedFactor);

        //Vector2 target = new Vector2(player.position.x, transform.position.y);

        //rb.velocity = direction * new Vector2(normalSpeed, 0);

        //transform.position = Vector2.MoveTowards(transform.position, target, normalSpeed * Time.fixedDeltaTime);
        
    }

    public void LookAtPlayer()
    {
        if (transform.position.x > player.position.x && isFlipped)
        {
            isFlipped = false;
            Flip();
        } else if (transform.position.x < player.position.x && !isFlipped)
        {
            isFlipped = true;
            Flip();
        }
    }

    public void Flip()
    {
        direction *= -1;
        transform.Rotate(0f, 180f, 0f);
    }
}
