﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExecutionAttack : MonoBehaviour
{
    ExecutionAttackInput input;
    public float attackRate = 2f;
    private float nextAttackTime = 0f;
    public float distanceToGround = 30;
    public Vector2 separationGround = new Vector2(0f, 0.3f);
    public LayerMask groundLayer; 
    public LayerMask enemyLayer;
    public float deltaRange = 1f;
    PlayerMovement movement;
    Rigidbody2D rigidBody; 
    public float newGravity = 5;
    [HideInInspector] public Vector2 hitPoint;
    [HideInInspector] public Vector2 playerPos;
    [HideInInspector] public bool isInExecution = false;
    [HideInInspector] public bool isEnemyDetected = false;
    [HideInInspector] public float previous_gravity;
    private Enemy collEnem;

    // Start is called before the first frame update
    void Start()
    {
        input = GetComponent<ExecutionAttackInput>();
        movement = GetComponent<PlayerMovement>();
        rigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        RaycastHit2D hit = Physics2D.Raycast(rigidBody.position, Vector2.down, distanceToGround, groundLayer);
        playerPos = rigidBody.position;
        if(hit){
            hitPoint = hit.point + separationGround;
            RaycastHit2D hit1Dir = Physics2D.Raycast(hitPoint, Vector2.right, deltaRange, enemyLayer);
            RaycastHit2D hit2Dir = Physics2D.Raycast(hitPoint, Vector2.left, deltaRange, enemyLayer);
            if(Time.time >= nextAttackTime){
                if(hit1Dir || hit2Dir){
                    isEnemyDetected = true;
                    if(input.attackExPressed && !movement.isTotallyOnGround && rigidBody.velocity.y<0){
                        isInExecution = true;
                        previous_gravity = rigidBody.gravityScale;
                        rigidBody.gravityScale = rigidBody.gravityScale * newGravity;
                        if(hit1Dir && hit2Dir){
                            GameObject collEnem1 = hit1Dir.collider.gameObject;
                            GameObject collEnem2 = hit2Dir.collider.gameObject;
                            float distance1 = Mathf.Abs(collEnem1.transform.position.x - playerPos.x);
                            float distance2 = Mathf.Abs(collEnem2.transform.position.x - playerPos.x);
                            if(distance1 < distance2 ){
                                collEnem = hit1Dir.collider.gameObject.GetComponent<Enemy>();
                            }else{
                                collEnem = hit2Dir.collider.gameObject.GetComponent<Enemy>();
                            }
                        }else if(hit1Dir){
                            collEnem = hit1Dir.collider.gameObject.GetComponent<Enemy>();
                        }else{
                            collEnem = hit2Dir.collider.gameObject.GetComponent<Enemy>();
                        }
                        nextAttackTime = Time.time + 1 / attackRate;
                    }
                }else{
                    isEnemyDetected = false;
                }    
            }
        }
        if(isInExecution && movement.isTotallyOnGround){
            collEnem.TakeDamage(collEnem.maxHealth);
            isInExecution = false;
            rigidBody.gravityScale = previous_gravity;
        }
    }
    public void OnDrawGizmos() {
        Gizmos.color = isEnemyDetected ? Color.green : Color.red;
        Gizmos.DrawSphere(hitPoint, 0.2f);
        Gizmos.DrawLine(playerPos, hitPoint);
        Gizmos.DrawLine(hitPoint, hitPoint + (Vector2.right*deltaRange));
        Gizmos.DrawLine(hitPoint, hitPoint + (Vector2.left*deltaRange));
    }
}
