﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExecutionAttackAnimations : MonoBehaviour
{
    ExecutionAttack executer;
    Animator anim;  
    int executingAttackParamID; 
    void Start()
    {
        executingAttackParamID = Animator.StringToHash("isInExecution");
        
        executer = GetComponent<ExecutionAttack>();
        anim = GetComponent<Animator>();

        if(executer == null || anim == null){
            Debug.LogError("A needed component is missing from the player");
            Destroy(this);
        }
    }
    void Update() {
        anim.SetBool(executingAttackParamID, executer.isInExecution);
    }

}
