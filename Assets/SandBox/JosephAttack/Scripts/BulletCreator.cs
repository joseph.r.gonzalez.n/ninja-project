﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCreator : MonoBehaviour
{
    public GameObject originalBullet; 
    public GameObject container;
    public LayerMask playerMask;
    public GameObject player;
    // public float vertical_speed = 5f;
    public float horizontal_speed = 17f;
    public float frequency_bullet = 2f;
    public float radius_attack = 20f;
    float time_counter;
    bool isEnemyDetected = false;
    Transform transform_enemy;
    Transform transform_player;
    Enemy_Movement ene_Movement;
    Rigidbody2D rigidBody; 
    private int direction = 1; 

    void Start(){
        transform_enemy = GetComponent<Transform>();
        transform_player = player.GetComponent<Transform>();
        ene_Movement = GetComponent<Enemy_Movement>();
        rigidBody = GetComponent<Rigidbody2D>();
    }
    void FixedUpdate(){
        Collider2D[] playerCheck = Physics2D.OverlapCircleAll(transform_enemy.position, radius_attack, playerMask);
        if (playerCheck.Length > 0){
            if(!isEnemyDetected){
                time_counter = Time.time;
            }
            isEnemyDetected = true;
            if(Time.time > time_counter){
                LaunchProjectile();
                time_counter += frequency_bullet;
            }
        }else{
            isEnemyDetected = false;
        }
    }

    void LaunchProjectile(){
        GameObject projectile = Instantiate(originalBullet);
        direction = ene_Movement.isFlipped ? 1:-1;
        projectile.GetComponent<Bullet>().owner = gameObject;
        projectile.transform.parent = container.transform;
        projectile.transform.position = transform_enemy.position + new Vector3(1f*direction,0,0);
        float voy = followPlayer();
        projectile.GetComponent<Rigidbody2D>().velocity = new Vector2(horizontal_speed*direction,voy);
    }

    float followPlayer(){
        float ppx = Mathf.Abs(transform_player.position.x - transform_enemy.position.x);
        float ppy = transform_player.position.y - transform_enemy.position.y;
        float ace = Physics.gravity.y;
        float vox = horizontal_speed;
        float voy = (ppy*vox/ppx) - (ace*ppx)/(2*vox);
        return voy;
    }
    public void OnDrawGizmos() {
        Gizmos.color = isEnemyDetected ? Color.green : Color.red;
        if(transform_enemy != null){
            Gizmos.DrawWireSphere(transform_enemy.position, radius_attack);
        }  
    }
}
