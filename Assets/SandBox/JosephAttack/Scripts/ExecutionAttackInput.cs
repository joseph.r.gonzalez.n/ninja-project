﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-100)]
public class ExecutionAttackInput : PlayerInput
{
    [HideInInspector] public bool attackExPressed;
    // Start is called before the first frame update
    public override void ClearInput()
    {
        //If we're not ready to clear input, exit
        if (!readyToClear)
            return;

        //Reset all inputs
        attackExPressed = false;
    }

    public override void ProcessInputs()
    {
        //Accumulate button inputs
        attackExPressed = attackExPressed || Input.GetButtonDown("AttackExecution");

    }
}
