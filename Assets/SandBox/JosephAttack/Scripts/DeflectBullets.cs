﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeflectBullets : MonoBehaviour
{
    Transform playerTransform;
    public LayerMask bulletMask;
    PlayerAttackInput input;
    PlayerMovement playerMovement;
    public float attackRate = 2f;
    private float nextAttackTime = 0f;
    public float innerRadius = 5f;
    public float outerRadius = 10f;
    bool isInInner = false;
    bool isInOuter = false;
    Vector2 enemyPosition;
    Vector2 bulletVelocity;
    // Start is called before the first frame update
    void Start()
    {
        input = GetComponent<PlayerAttackInput>();
        playerTransform = GetComponent<Transform>();
        playerMovement = GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Collider2D[] innerCheck = Physics2D.OverlapCircleAll(playerTransform.position, innerRadius, bulletMask);
        Collider2D[] outerCheck = Physics2D.OverlapCircleAll(playerTransform.position, outerRadius, bulletMask);
        float direction = playerMovement.direction;
        if(innerCheck.Length > 0){
            isInInner = true;
            isInOuter = false;
            if(input.attackInstant && Time.time >= nextAttackTime){
                enemyPosition = innerCheck[0].GetComponent<Bullet>().owner.transform.position;
                bulletVelocity = innerCheck[0].GetComponent<Rigidbody2D>().velocity;
                innerCheck[0].GetComponent<Rigidbody2D>().velocity = new Vector2(-bulletVelocity.x,followPlayer());
                nextAttackTime = Time.time + 1 / attackRate;
            }            
        }else if(outerCheck.Length > 0){
            isInInner = false;
            isInOuter = true;
            if(input.attackInstant && Time.time >= nextAttackTime){
                outerCheck[0].GetComponent<Rigidbody2D>().velocity = new Vector2(Random.Range(0, 20.0f)*direction,Random.Range(0, 20.0f));
                nextAttackTime = Time.time + 1 / attackRate;
            }  
        }else{
            isInInner = false;
            isInOuter = false;
        }  
    }

    float followPlayer(){
        float ppx = Mathf.Abs(enemyPosition.x - playerTransform.position.x);
        float ppy = enemyPosition.y - playerTransform.position.y;
        float ace = Physics.gravity.y;
        float vox = bulletVelocity.x;
        float voy = (ppy*vox/ppx) - (ace*ppx)/(2*vox);
        return voy;
    }
    public void OnDrawGizmos() {
        if(playerTransform != null){
            Gizmos.color = isInInner ? Color.green : Color.red;
            Gizmos.DrawWireSphere(playerTransform.position, innerRadius);
            Gizmos.color = isInOuter ? Color.green : Color.red;
            Gizmos.DrawWireSphere(playerTransform.position, outerRadius);
        }  
    }

}
