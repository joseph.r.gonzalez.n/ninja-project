﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol_Behaviour1 : StateMachineBehaviour
{
    Enemy_Movement flip;


    float currentTime = 0;
    public float timeToTurn = 2f;
    public float speed;
   
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        flip = animator.GetComponent<Enemy_Movement>();
        animator.SetBool("IsPatrol", false);
        

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(timeToTurn <= 0)
        {
            speed = -speed;
            timeToTurn = 2f;
        }


        if (speed < 0)
        {
            animator.transform.Translate(Vector2.right * speed * Time.deltaTime);
            animator.transform.eulerAngles = new Vector3(0f, 0f, 0f);
            flip.isFlipped = false;


        }

        if (speed > 0)
        {
            animator.transform.Translate(Vector2.right * -speed * Time.deltaTime);
            animator.transform.eulerAngles = new Vector3(0f, 180f, 0f);
            flip.isFlipped = true;




        }

     
        timeToTurn -= Time.deltaTime;



    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
       
       //flip.isFlipped = !flip.isFlipped;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
