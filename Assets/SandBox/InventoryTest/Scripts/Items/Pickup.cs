﻿using UnityEngine;

public class Pickup : MonoBehaviour
{
    public Item item;

    public void Start()
    {

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Picking up " + item.name);
            bool wasPickedUp = Inventory.instance.Add(item);
            //add item to inventory
            if (wasPickedUp)
            {
                Destroy(gameObject);
            }
        }
    }
}
