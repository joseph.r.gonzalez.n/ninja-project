﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Movement : MonoBehaviour
{
    public Transform playerPos;
    public Animator animator;
    public Transform chaseCheck;
    public Collider2D[] playerCheck;
    public LayerMask playerMask;


    float radiusCheck;
    


    public bool isFlipped = false;


    private void Start()
    {
        radiusCheck = GetComponent<BulletCreator>().radius_attack;
        if(playerPos == null)
        {
            Transform player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
            playerPos = player;
        }
     



    }



    private void Update()
    {
        Chase();


        //if(timeToTurn <= 0)
        //{
        //    speed = -speed;
        //    timeToTurn = 2f;
        //}

        //timeToTurn -= Time.deltaTime;



    }



    public void Chase()
    {

        playerCheck = Physics2D.OverlapCircleAll(chaseCheck.position, radiusCheck, playerMask);

        if (playerCheck.Length > 0)
        {
            animator.SetBool("IsChasing", true);
        }
        else
        {
            animator.SetBool("IsChasing", false);
        }




    }




    public void LookAtPlayer()
    {
        Vector3 flipped = transform.localScale;
        flipped.z *= -1f;

        if (transform.position.x > playerPos.position.x && isFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            isFlipped = false;

        }
        else if (transform.position.x < playerPos.position.x && !isFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            isFlipped = true;
        }


    }



    private void OnDrawGizmosSelected()
    {
        if (chaseCheck == null)
            return;
        Gizmos.DrawWireSphere(chaseCheck.position, radiusCheck);
    }




}



