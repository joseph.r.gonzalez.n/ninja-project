﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move_Platform : MonoBehaviour
{
    public float timer;
    public float newTimer;
    public float speed;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.up * speed * Time.deltaTime);

        if (timer <= 0)
        {
            speed = -speed;
            timer = newTimer;
        }
        timer -= Time.deltaTime;

        

    }
}
