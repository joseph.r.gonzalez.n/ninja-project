﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cinta_transportadora : MonoBehaviour
{
    public float speed;
    private void OnTriggerStay2D(Collider2D collision)
    {
        Rigidbody2D playerRb = collision.GetComponent<Rigidbody2D>();
        playerRb.velocity = new Vector2(0f,playerRb.velocity.y);
        playerRb.AddForce(new Vector2(speed, 0f), ForceMode2D.Force);
        Debug.Log(playerRb);
        
    }
}
