﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformControl : MonoBehaviour
{
    public Vector3 offset;
    public Vector3 initialPosition;
    public float speed;

    void Start()
    {
        initialPosition = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {


        Vector2 upPosition = Vector2.MoveTowards(transform.position, initialPosition + offset, speed * Time.deltaTime);

        if (transform.position == initialPosition + offset)
        {
            offset = -offset;
        }


        transform.position = upPosition;

    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        collision.collider.transform.SetParent(transform);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        collision.collider.transform.SetParent(null);
    }


}
