﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour
{


    public ParticleSystem dashtrail1;
    public ParticleSystem jumpDust;
    public CharacterController2D controller;
    public Joystick joystick;
    public Animator animator;


    public float runSpeed = 40f;
    public float horizontalMove = 0f;
    public bool jump = false;
    private bool crouch = false;
    private bool dash = false;


    //touch controllers
    public bool touchJump = false;
    public bool touchDash = false;


    public float jumpRate = 2f;
    public float dashRate = 2f;

    float nextJumptime = 0f;
    float nextTimetoDash = 0f;
    float dashDuration = 0f;

    public bool secondJump = false;
    public bool attack = false;
    private bool grounded;
    void Start()
    {
        


        //if(joystick == null)
        //{
        //    Joystick playerJoystick = GameObject.FindGameObjectWithTag("Joystick").GetComponent<Joystick>();
        //    joystick = playerJoystick;

        //}
    }

    // Update is called once per frame
    void Update()

      
    {


        grounded = GetComponent<CharacterController2D>().m_Grounded;

        // for keyboard input

        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed; 

        // for joystick input
        //horizontalMove = joystick.Horizontal * runSpeed;
        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));
        
        

        //for jumping
        
        if(Time.time >= nextJumptime)
        {
            if (Input.GetButtonDown("Jump") || touchJump)
            {

                jumpDust.Play();
                animator.SetBool("IsJumping", true);
                nextJumptime = Time.time + 1f / jumpRate;
                jump = true;
                touchJump = false;



                if(secondJump)
                {
                    //Debug.Log("roll");
                    animator.SetBool("IsJumping", false);
                    animator.SetBool("IsRolling", true);
                    secondJump = false;
                }
                

            }
        }
        //for dashing
        if (Time.time >= nextTimetoDash)
        {
            if (Input.GetButtonDown("Dash") || touchDash)
            {
                //animator.SetTrigger("IsDashing");

                //if (joystick.Horizontal > 0.1)
                //{
                //    dashtrail1.GetComponent<ParticleSystemRenderer>().flip = new Vector3(0, 0, 0);
                //    dashtrail1.Play();

                //}else if(joystick.Horizontal < -0.1)
                //{
                //    dashtrail1.GetComponent<ParticleSystemRenderer>().flip = new Vector3(1, 0, 0);
                //    dashtrail1.Play();
                //}

                
                dash = true;
                nextTimetoDash = Time.time + 1f / dashRate;
                dashDuration = Time.time + 1f / 4;
                touchDash = false;
            }



        }

        if ( grounded == false)
        {
            Falling();
        }


        

        
    }



    void FixedUpdate()
    {

        controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump, dash);
        jump = false;
        if (Time.time >= dashDuration)
        {
            dash = false;
            
        }
            
    }

    public void Landing()
    {
        animator.SetBool("IsJumping", false);
        animator.SetBool("IsRolling", false);
        animator.SetBool("IsFalling",false);  
    }


    void Falling()
    {
        animator.SetBool("IsFalling", true);
    }

}
