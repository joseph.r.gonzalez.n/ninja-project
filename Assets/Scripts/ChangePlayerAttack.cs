﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePlayerAttack : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            PlayerCombat attack = collision.GetComponent<PlayerCombat>();

            attack.IsRangeAttack = true;
            Destroy(gameObject);

        }
        
    }
}
