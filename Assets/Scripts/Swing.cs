﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class Swing : MonoBehaviour
{
    public HingeJoint2D rope;
    public Transform ropePoint;
    public float radio;
    public LayerMask ropes;
    public bool IsInRope;

  


    private void Start()
    {
        rope = GetComponent<HingeJoint2D>();
    }


    private void Update()
    {


        if (!IsInRope)
        {
            Collider2D[] Arope = Physics2D.OverlapCircleAll(ropePoint.position, radio, ropes);

            foreach (Collider2D cuerdas in Arope)
            {
                if (cuerdas.tag == "Rope")
                {
                    rope.enabled = true;
                    rope.connectedBody = cuerdas.GetComponent<Rigidbody2D>();
                    GetComponent<CharacterController2D>().m_Grounded = true;
                    GetComponent<Rigidbody2D>().freezeRotation = false;
                    //animation in the rope
                    GetComponent<Player_Controller>().Landing();
                    IsInRope = true;
                }

            }

        }
        
       

        if (Input.GetButtonDown("Jump") && IsInRope)
        {
            rope.enabled = false;
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 0f);
            GetComponent<Rigidbody2D>().freezeRotation = true;
            IsInRope = false;
        }


        //if (IsInRope)
        //{
        //    if(timeOutRope <= 0)
        //    {
        //        IsInRope = false;
        //    }

        //}




    
}
    



    private void OnDrawGizmosSelected()
    {
        if (ropePoint == null)
            return;
        Gizmos.DrawWireSphere(ropePoint.position, radio);
    }

}
