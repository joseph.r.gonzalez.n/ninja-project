﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public GameObject VFXdamage;

    public int maxPlayerHealth = 100;
    //public HealthBarScript healthBar;
    //public GameMaster gameMaster;
    Animator animator;
    int hitParamID;
    

    public int currentHealth;

    void Start()
    {

        animator = GetComponent<Animator>();
        hitParamID = Animator.StringToHash("isHit");


        //if(healthBar == null)
        //{
        //    HealthBarScript healthplayer = GameObject.FindGameObjectWithTag("Health_Bar").GetComponent<HealthBarScript>();
        //    healthBar = healthplayer;
        //}

        //if(gameMaster == null)
        //{
        //    GameMaster gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();
        //    gameMaster = gm;
        //}

        currentHealth = maxPlayerHealth;
        //healthBar.SetMaxHealth(maxPlayerHealth);

    }




    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        animator.SetTrigger(hitParamID);
        Instantiate(VFXdamage, transform.position, transform.rotation);
        //healthBar.SetHealth(currentHealth);

        //Debug.Log("hit");

        if (currentHealth <= 0)
        {
            //Player Dies Die()
            //gameMaster.GetComponent<GameMaster>().EndGame();
            //gameMaster.GetComponent<GameMaster>().KillPlayer();

            Debug.Log("Player is Dead");
        }

    }




   
}
