﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimberGame : MonoBehaviour
{
    public Transform player;
    public GameObject endpanel;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()

    {
        PlatformControl control = GameObject.FindGameObjectWithTag("MovePlatform").GetComponent<PlatformControl>();
        if (player.transform.position.y >= 10f && player.transform.position.y  <= 20f)
        {

            control.speed = 5f;
        }else if(player.transform.position.y >= 20f && player.transform.position.y <= 40f)
        {
            control.speed = 10f;
        }

        if (player.transform.position.y <= -1f)
        {
            EndGame();
        }




    }


    void EndGame()
    {
        Time.timeScale = 0;
        endpanel.SetActive(true);

    }


}
