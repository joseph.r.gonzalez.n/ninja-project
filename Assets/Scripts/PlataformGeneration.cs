﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformGeneration : MonoBehaviour
{
    public GameObject plataforma;
    public Vector3 offset;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector3 desiredPosition = new Vector3(0f, transform.position.y, transform.position.z);


        Instantiate(plataforma, desiredPosition + offset, Quaternion.identity);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        Destroy(gameObject);
    }



}

