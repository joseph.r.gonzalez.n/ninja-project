﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGeneratorController : MonoBehaviour
{
    public float timetoGenerate;
    public float timerTime;
    public int levelsize;

    private GameObject[] rooms;
    public int numberOfRooms;
    public bool endofLevel = false;


    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
         
        rooms = GameObject.FindGameObjectsWithTag("Room");
        numberOfRooms = rooms.Length;
        if (numberOfRooms <= levelsize)
        {
            if (timetoGenerate <= 0)
            {
                timetoGenerate = timerTime;
                //levelsize -= 1;
            }
            timetoGenerate -= Time.deltaTime;
        }
        else
        {
            StartCoroutine(endGeneration());
            timetoGenerate = timerTime;
        }
       
    }


    IEnumerator endGeneration()
    {
        yield return new WaitForSeconds(2);
        endofLevel = true;
    }
}
