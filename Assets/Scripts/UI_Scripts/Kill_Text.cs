﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Kill_Text : MonoBehaviour
{
    public int kills = 0;
    public Text killText;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        kills = GameMaster.DeadEnemiesUI;
        killText.text = "Kills:" + " " + kills;
    }
}
