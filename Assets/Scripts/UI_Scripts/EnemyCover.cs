﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCover : MonoBehaviour
{

    //references
    EnemyStats enemyStats;

    //enemy movement
    Rigidbody2D rbEnemy;

    //variable to detect player for cover
    public Transform rayPosition;
    public LayerMask playerLayer;
    public float rayLength;
    public RaycastHit2D detectplayer;
    public GameObject covereffect;


    //movement
    public float timetoFlip;
    public int direction = 1;




    void Start()
    {
        rbEnemy = GetComponent<Rigidbody2D>();
        enemyStats = GetComponent<EnemyStats>();
    }




    void Update()
    {
        EnemyMovement();
        DetectPlayer();
    }

    void DetectPlayer()
    {
        detectplayer = Physics2D.Raycast(rayPosition.position, Vector2.right * direction, rayLength, playerLayer);
        if (detectplayer)
        {
            //Debug.Log("Hit");
        }
        
        Debug.DrawRay(rayPosition.position, Vector2.right * rayLength * direction, Color.yellow);
    }



    void EnemyMovement()
    {

        rbEnemy.velocity = Vector2.right * enemyStats.speed * direction;

        if(timetoFlip <= 0)
        {
            FlipCharacterDirection();
            timetoFlip = 2f;
        }

        timetoFlip -= Time.deltaTime;

    }



    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<PlayerStats>().TakeDamage(enemyStats.attackDamage);
        }
    }



    void FlipCharacterDirection()
    {
        //Turn the character by flipping the direction
        direction *= -1;

        //Flip the parent
        transform.Rotate(0f, 180f, 0f);
        
    }




    public void CoverAttack()
    {
        Instantiate(covereffect, transform.position, transform.rotation);

        //execute cover animation
        //excute sound
    }



}
