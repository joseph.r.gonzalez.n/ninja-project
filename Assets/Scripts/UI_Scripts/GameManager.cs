﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject endmenu;

    PlayerStats stats;

    // Start is called before the first frame update
    void Start()
    {
        stats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();

    }

    // Update is called once per frame
    public void Update()
    {

        if(Input.GetKeyDown("r"))
        {
            RestartScene();
        }

        
        if(stats.currentHealth == 0)
        {
            EndGame();
        }

    }



    public void EndGame()
    {
        endmenu.SetActive(true);
        Time.timeScale = 0;
    }


    public void RestartScene()
    {
        
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }

}
