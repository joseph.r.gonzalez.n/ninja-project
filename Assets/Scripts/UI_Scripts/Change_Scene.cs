﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Change_Scene : MonoBehaviour

{
    //variable to move the camera and reset follow and confiner to next scene
    [Header("Camera Transition")]
    public Collider2D rightcameraConfinder;
    public Collider2D leftcameraConfinder;
    
    Rigidbody2D player;
    CinemachineVirtualCamera followPlayerCamera;
    CinemachineConfiner levelConfinder;
    [Header("Scene Transition")]

    //roms to switch
    public GameObject roomLeft;
    public GameObject roomRight;




    void Start()
    {
        followPlayerCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CinemachineVirtualCamera>();
        levelConfinder = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CinemachineConfiner>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
       //add transition animation disable player controllers
        followPlayerCamera.PreviousStateIsValid = false;
        //player going right
        if(player.velocity.x > 1)
        {
            levelConfinder.m_BoundingShape2D = rightcameraConfinder;
            roomRight.SetActive(true);
            roomLeft.SetActive(false);

        }//player going left
        else
        {
            levelConfinder.m_BoundingShape2D = leftcameraConfinder;
            roomRight.SetActive(false);
            roomLeft.SetActive(true);
        }

        
        levelConfinder.InvalidatePathCache();


    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (player.velocity.x > 1)
        {
            levelConfinder.m_BoundingShape2D = rightcameraConfinder;
            roomRight.SetActive(true);
            roomLeft.SetActive(false);

        }//player going left
        else
        {
            levelConfinder.m_BoundingShape2D = leftcameraConfinder;
            roomRight.SetActive(false);
            roomLeft.SetActive(true);
        }

        levelConfinder.InvalidatePathCache();
        // enable player controllers


    }

}
