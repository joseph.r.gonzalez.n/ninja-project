﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_Constructor : MonoBehaviour
{
    public bool levelBegin = true;
    public bool levelContinuity = false;
    public bool nextPart = false;
    public bool current = true;

    public int partCount = 10;
    public float offset = 50f;

    public GameObject[] levelParts;
    public List<GameObject> nextParts;
    public GameObject initialPart;
    public GameObject previousPart; 
    public GameObject currentPart;
    public GameObject futurePart;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (levelBegin)
        {
            LevelBegin();
        }

        if (levelContinuity && partCount >= 0)
        {
            LevelContinuity();
        }

    }




    void LevelBegin()
    {
        int initialPartSelector = Random.Range(0, levelParts.Length);
        Vector3 origin = new Vector3(0f, 0f, 0f);
        initialPart = levelParts[0];
        currentPart = Instantiate(initialPart, origin, Quaternion.identity);
        previousPart = currentPart;
        levelBegin = false;
        levelContinuity = true;


    }

    void LevelContinuity()
    {
        if(currentPart.tag == "Corridor")
        {
            if(previousPart.tag == "Corridor")
            {
                Debug.Log("Corridor");
                foreach (GameObject part in levelParts)
                {
                    if (part.tag != "Right_Up" && part.tag != "Right_Down" && part.tag != "Corridor")
                    {
                        nextParts.Add(part);

                    }

                }

                int randomPartSelector = Random.Range(0, nextParts.Count);
                Vector3 desiredPosition = new Vector3(currentPart.transform.position.x + 50f, currentPart.transform.position.y, currentPart.transform.position.z);
                futurePart = Instantiate(nextParts[randomPartSelector], desiredPosition, Quaternion.identity);
                nextParts.Clear();
                previousPart = currentPart;
            }
            else if (previousPart.tag == "Left_Down" || previousPart.tag == "Left_Up")
            {
                foreach (GameObject part in levelParts)
                {
                    if (part.tag == "Right_Up" && part.tag == "Corridor")
                    {
                        nextParts.Add(part);

                    }

                }
                int randomPartSelector = Random.Range(0, nextParts.Count);
                Vector3 desiredPosition = new Vector3(currentPart.transform.position.x - 50f, currentPart.transform.position.y, currentPart.transform.position.z);
                futurePart = Instantiate(nextParts[randomPartSelector], desiredPosition, Quaternion.identity);
                nextParts.Clear();
                previousPart = currentPart;
            }else if(previousPart.tag == "Right_Down" || previousPart.tag == "Right_Up")
            {
                foreach (GameObject part in levelParts)
                {
                    if (part.tag == "Left_Up" && part.tag == "Corridor")
                    {
                        nextParts.Add(part);

                    }

                }
                int randomPartSelector = Random.Range(0, nextParts.Count);
                Vector3 desiredPosition = new Vector3(currentPart.transform.position.x - 50f, currentPart.transform.position.y, currentPart.transform.position.z);
                futurePart = Instantiate(nextParts[randomPartSelector], desiredPosition, Quaternion.identity);
                nextParts.Clear();
                previousPart = currentPart;

            }
            currentPart = futurePart;

        }
        //LEFT_UP

        else if(currentPart.tag == "Left_Up")
        {
            
            if(previousPart.tag == "Corridor" || previousPart.tag == "Right_Up" || previousPart.tag == "Right_Down")
            {
                Debug.Log("Left_Up_From_Corridor");
                foreach (GameObject part in levelParts)
                {
                    if (part.tag == "Left_Down" || part.tag == "Righ_Down")
                    {
                        nextParts.Add(part);
                    }
                }

                int randomPartSelector = Random.Range(0, nextParts.Count);
                Vector3 desiredPosition = new Vector3(currentPart.transform.position.x, currentPart.transform.position.y + 50f, currentPart.transform.position.z);
                futurePart = Instantiate(nextParts[randomPartSelector], desiredPosition, Quaternion.identity);
                nextParts.Clear();
                previousPart = currentPart;
            }else if(previousPart.tag == "Left_Down" || previousPart.tag == "Right_Down")
            {
                Debug.Log("Other_left_up");
                foreach (GameObject part in levelParts)
                {
                    if (part.tag == "Corridor" || part.tag == "Righ_Down")
                    {
                        nextParts.Add(part);
                    }
                }

                int randomPartSelector = Random.Range(0, nextParts.Count);
                Vector3 desiredPosition = new Vector3(currentPart.transform.position.x - 50f, currentPart.transform.position.y, currentPart.transform.position.z);
                futurePart = Instantiate(nextParts[randomPartSelector], desiredPosition, Quaternion.identity);
                nextParts.Clear();
                previousPart = currentPart;

            }
            
            currentPart = futurePart;


        }
        else if(currentPart.tag == "Left_Down")
        {
            
            if (previousPart.tag == "Corridor" || previousPart.tag == "Right_Up" || previousPart.tag == "Right_Down")
            {
                Debug.Log("Left_Down_from_corridor");
                foreach (GameObject part in levelParts)
                {
                    if (part.tag == "Right_Up" || part.tag == "Left_Up")
                    {
                        nextParts.Add(part);
                    }
                }
                int randomPartSelector = Random.Range(0, nextParts.Count);
                Vector3 desiredPosition = new Vector3(currentPart.transform.position.x, currentPart.transform.position.y - 50f, currentPart.transform.position.z);
                futurePart = Instantiate(nextParts[randomPartSelector], desiredPosition, Quaternion.identity);
                nextParts.Clear();
                previousPart = currentPart;

            }else if(previousPart.tag == "Left_Up" || previousPart.tag == "Right_Up")
            {
                Debug.Log("Other_left_down");
                foreach (GameObject part in levelParts)
                {
                    if (part.tag == "Right_Up" || part.tag == "Corridor")
                    {
                        nextParts.Add(part);
                    }
                }
                int randomPartSelector = Random.Range(0, nextParts.Count);
                Vector3 desiredPosition = new Vector3(currentPart.transform.position.x - 50f, currentPart.transform.position.y, currentPart.transform.position.z);
                futurePart = Instantiate(nextParts[randomPartSelector], desiredPosition, Quaternion.identity);
                nextParts.Clear();
                previousPart = currentPart;
            }
            
            currentPart = futurePart;




        }
        else if(currentPart.tag == "Right_Up")
        {
            
            if(previousPart.tag == "Corridor")
            {
                Debug.Log("Right_Up_from_corridor");
                foreach (GameObject part in levelParts)
                {
                    if (part.tag == "Right_Down" || part.tag == "Left_Down")
                    {
                        nextParts.Add(part);
                    }
                }
                int randomPartSelector = Random.Range(0, nextParts.Count);
                Vector3 desiredPosition = new Vector3(currentPart.transform.position.x, currentPart.transform.position.y + 50f, currentPart.transform.position.z);
                futurePart = Instantiate(nextParts[randomPartSelector], desiredPosition, Quaternion.identity);
                nextParts.Clear();
                previousPart = currentPart;
            }else if(previousPart.tag == "Right_Down" || previousPart.tag == "Left_Down")
            {
                Debug.Log("Right_Up_from_not_corridor");
                foreach (GameObject part in levelParts)
                {
                    if (part.tag == "Corridor" || part.tag == "Left_Down")
                    {
                        nextParts.Add(part);
                    }
                }
                int randomPartSelector = Random.Range(0, nextParts.Count);
                Vector3 desiredPosition = new Vector3(currentPart.transform.position.x + 50f, currentPart.transform.position.y, currentPart.transform.position.z);
                futurePart = Instantiate(nextParts[randomPartSelector], desiredPosition, Quaternion.identity);
                nextParts.Clear();
                previousPart = currentPart;

            }
            currentPart = futurePart;


        }
        else if(currentPart.tag == "Right_Down")
        {
            if(previousPart.tag == "Corridor")
            {
                Debug.Log("Right_Up_from_corridor");
                foreach (GameObject part in levelParts)
                {
                    if (part.tag == "Right_Up" || part.tag == "Left_Up")
                    {
                        nextParts.Add(part);
                    }
                }
                int randomPartSelector = Random.Range(0, nextParts.Count);
                Vector3 desiredPosition = new Vector3(currentPart.transform.position.x, currentPart.transform.position.y - 50f, currentPart.transform.position.z);
                futurePart = Instantiate(nextParts[randomPartSelector], desiredPosition, Quaternion.identity);
                nextParts.Clear();
                previousPart = currentPart;
            }else if(previousPart.tag == "Right_Up" || previousPart.tag == "Left_Up")
            {
                Debug.Log("Right_Up_not_from_corridor");
                foreach (GameObject part in levelParts)
                {
                    if (part.tag == "Corridor" || part.tag == "Left_Up")
                    {
                        nextParts.Add(part);
                    }
                }
                int randomPartSelector = Random.Range(0, nextParts.Count);
                Vector3 desiredPosition = new Vector3(currentPart.transform.position.x + 50f, currentPart.transform.position.y, currentPart.transform.position.z);
                futurePart = Instantiate(nextParts[randomPartSelector], desiredPosition, Quaternion.identity);
                nextParts.Clear();
                previousPart = currentPart;
            }
            currentPart = futurePart;

        }
        partCount -= 1;

    }



    void LevelEnd()
    { 


    }


}
