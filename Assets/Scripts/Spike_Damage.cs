﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike_Damage : MonoBehaviour
{
    //public PlayerHealth playerHealth;
    public int damage;
    public Vector2 force;
    private PlayerStats player;
    public float damageRateSet; 
    float damageRate;

    private void Start()
    {
        damageRate = 0f;
    }


    private void OnTriggerStay2D(Collider2D collision)
    {
        player = collision.GetComponent<PlayerStats>();
        
        if (player != null)
            {
            if(damageRate <= 0)
            {
                player.TakeDamage(damage);
                player.GetComponent<Rigidbody2D>().AddForce(force, ForceMode2D.Impulse);
                Debug.Log("Spikes");
                damageRate = damageRateSet;
            }

            damageRate -= Time.deltaTime;
                
            }
        
        

        

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        damageRate = 0f;
    }


}
