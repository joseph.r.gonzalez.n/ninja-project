﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchHook : MonoBehaviour
{

    //public SpringJoint2D hook;
    void Start()
    {
        //hook = GetComponent<SpringJoint2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {


            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
            if(hit.collider != null)
            {
                Vector3 desiredanchor = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
                //hook.connectedAnchor = desiredanchor;
                Debug.Log(hit.collider.gameObject.name);
            }
            

        }
    }
}
