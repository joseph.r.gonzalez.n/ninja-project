﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow_PLayer : MonoBehaviour
{
    public Transform target;

    public float smoothSpeed = 0.125f;
    public Vector3 offset;

   
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (target == null)
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            target = player.transform;
        }
         
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        transform.position = new Vector3(smoothedPosition.x, smoothedPosition.y, smoothedPosition.z);

    }
}

        
