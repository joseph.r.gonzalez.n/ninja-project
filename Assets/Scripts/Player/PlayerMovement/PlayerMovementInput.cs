﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovementInput : PlayerInput
{
    public float horizontal;      //Float that stores horizontal input
    [HideInInspector] public bool jumpHeld;         //Bool that stores jump pressed
    [HideInInspector] public bool jumpPressed;      //Bool that stores jump held
    [HideInInspector] public bool wallHold;
    //float horizontalUp = 0f;


    public override void ClearInput()
    {
        //If we're not ready to clear input, exit
        if (!readyToClear)
            return;

        // Reset all inputs

        //horizontal = Mathf.SmoothDamp(horizontal, 0f, ref horizontalUp, 0.1f);
        jumpPressed = false;
        jumpHeld = false;
        wallHold = false;
        

        readyToClear = false;
    }

    public override void ProcessInputs()
    {
        

        //Accumulate horizontal axis input
        horizontal = Input.GetAxisRaw("Horizontal");
        jumpPressed = jumpPressed || Input.GetButtonDown("Jump");
        jumpHeld = jumpHeld || Input.GetButton("Jump");
        wallHold = wallHold || Input.GetButton("WallHold");

    }

    public override void ExtraFunction()
    {
        
    }

   


}
