﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class PatrolBehaviourRange : StateMachineBehaviour
{
    //variables for patrol and random flip
    EnemyRangeAttack flipDirection;
    float currentTime = 0;
    public float timeToTurn;
    public float speed;
    public int backToIdle = 10;
    public float randomIdle = 0;

    //variables for groundDetection
    public float distance;
    public Transform groundDetection;





    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        flipDirection = animator.GetComponent<EnemyRangeAttack>();
        groundDetection = animator.transform.Find("GroundDetection").GetComponent<Transform>();


    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GroundDetection();

        if (timeToTurn <= 0)
        {
            //change the direction and 
            speed = -speed;
            timeToTurn = UnityEngine.Random.Range(1,5);

            randomIdle = UnityEngine.Random.Range(1, backToIdle);

            if (randomIdle >= 5)
            {
                animator.SetBool("IsPatrol", false);
                randomIdle = 0;
            }


        }

        if (speed < 0)
        {
            animator.transform.Translate(Vector2.right * speed * Time.deltaTime);
            animator.transform.eulerAngles = new Vector3(0f, 0f, 0f);
            flipDirection.isFlipped = false;


        }

        if (speed > 0)
        {
            animator.transform.Translate(Vector2.right * -speed * Time.deltaTime);
            animator.transform.eulerAngles = new Vector3(0f, 180f, 0f);
            flipDirection.isFlipped = true;




        }


        timeToTurn -= Time.deltaTime;
    }


    void GroundDetection()
    {
        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, distance);
        if (groundInfo.collider == null)
        {
            Debug.Log("ChangeDirection");
            speed = -speed;
        }
    }


    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }



    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
