﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Water_Behaviour : MonoBehaviour
{
    public Joystick joystick;
    public Animator floorAnimator;


    public float horizontalMove;
    public Vector2 newPos;
    private Vector3 originalPos;
    private bool hit = false;
    public float timeOnWater = 0;

    public float smoothSpeed;
    public int drops = 0;

    private void Awake()
    {

        originalPos = transform.position;

        if(joystick == null)
        {
            joystick = GameObject.FindGameObjectWithTag("Joystick").GetComponent<Joystick>();
        }
    }

    private void Update()
    {
        horizontalMove = joystick.Horizontal;

        

    }





    private void OnCollisionStay2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {
            
            //timeOnWater += Time.deltaTime;
            Debug.Log("Player in Water");
            if(Mathf.Abs(horizontalMove) == 0 && timeOnWater <= 0)
            {
                GetComponent<BoxCollider2D>().enabled = false;
                floorAnimator.SetBool("PlayerFall",true);
                timeOnWater = 0.1f;
               
                
            }
            timeOnWater -= Time.deltaTime;
        }
    }

      

}
