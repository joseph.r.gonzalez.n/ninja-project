﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class EnemyRange : MonoBehaviour
{
    public Transform hitPoint;
    public GameObject[] hits;
    


    public GameMaster gameMaster;
    public Animator animator;
    public int maxHealth = 100;
    public GameObject blood;
    //public CameraShake cameraShake;

    int currentHealth;


    void Start()
    {
        currentHealth = maxHealth;
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        Instantiate(blood, transform.position, blood.transform.rotation);
        animator.SetTrigger("Hit");

        CameraShaker.Instance.ShakeOnce(4f, 4f, .1f, 1f);
        //StartCoroutine(cameraShake.Shake(.15f, .4f));
        //Play hurt animation
        int hitRandom = Random.Range(0, hits.Length);
        Instantiate(hits[hitRandom], hitPoint.position, transform.rotation);




        if (currentHealth <= 0)
        {
            Die();
        }
    }

    void Die()
    {

        //Debug.Log("Enemy died");

        gameMaster.DeadEnemies += 1;

        animator.SetBool("IsDead", true);
        GetComponent<Collider2D>().enabled = false;
        GetComponent<EnemyRangeAttack>().enabled = false;
        this.enabled = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "PlayerMelee")
        {
            Debug.Log("Hit");
        }
    }



}
