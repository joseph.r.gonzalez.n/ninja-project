﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotarSprite : MonoBehaviour
{
    public float speed = 1f;

    private void Update()
    {
        transform.Rotate(0f, 0f, speed);
    }

}

