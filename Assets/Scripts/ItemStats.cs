﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class ItemStats : MonoBehaviour
{
    public int maxHealth = 100;
    public ParticleSystem itemParts;
    public Sprite damageCrate;
    public int currentHealth;
    public GameObject hitDust;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
    }

    

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        itemParts.Play();
        CameraShaker.Instance.ShakeOnce(4f, 4f, .1f, 1f);
        Instantiate(hitDust, transform.position, Quaternion.identity);
        //StartCoroutine(cameraShake.Shake(.15f, .4f));


        if(currentHealth <= 30 && currentHealth >= 0)
        {
            GetComponent<SpriteRenderer>().sprite = damageCrate;
        }

        if (currentHealth <= 0)
        {
            Destroy();
        }
    }

    void Destroy()
    {

        Destroy(this.gameObject);
        this.enabled = false;
    }



}
