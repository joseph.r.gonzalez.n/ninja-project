﻿
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Timeline;

public class ParallaxEffect : MonoBehaviour
{

    public GameObject player;
    private Vector3 originalPosition;
    private Vector3 distance;
    private Vector3 entrancePosition;
    public Vector2 parallaxvelocity;


    public bool entrance = true;

    public float playerSpeed;

    public float distX;
    public float distY;
    


    void Start()
    {
        originalPosition = transform.position;
        if(player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
       
           
    }

    // Update is called once per frame
    void Update()
    {


        distance = player.transform.position - originalPosition;

        if(Mathf.Abs(distance.x) <= 35)
        {
            distX = (player.transform.position.x * parallaxvelocity.x);
            distY = (player.transform.position.y * parallaxvelocity.y);

            if (entrance)
            {
                entrancePosition = new Vector3(player.transform.position.x - distX - distance.x, player.transform.position.y + distY - distance.y);
                entrance = false;
            }
            

            Vector3 desiredpositionX = new Vector3(entrancePosition.x + distX, 0f);
            Vector3 desiredPositionY = new Vector3(0f, entrancePosition.y - distY);

            transform.position = desiredpositionX + desiredPositionY;
        }
        else
        {
            transform.position = originalPosition;
            entrance = true;
        }
        


        

    }

   



}
