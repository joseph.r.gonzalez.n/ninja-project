﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Enemy : MonoBehaviour
{
    // public GameMaster gameMaster;
    public Animator animator;
    public int maxHealth = 100;
    public GameObject Hiteffect;
    //public CameraShake cameraShake;
    public CinemachineImpulseSource shake;



    int currentHealth;

    void Awake()
    {
        shake = GetComponent<CinemachineImpulseSource>();
    }


    void Start()
    {
        currentHealth = maxHealth;
        // if(gameMaster == null)
        // {
        //     gameMaster = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();
        // }
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        Instantiate(Hiteffect, transform.position, transform.rotation);
        shake.GenerateImpulse();
        //animator.SetTrigger("Hit");

        //StartCoroutine(cameraShake.Shake(.15f, .4f));
        //Play hurt animation

        if(currentHealth <=0)
        {
            Die();
        }
    }
    
    void Die()
    {

        //Debug.Log("Enemy died");

        //gameMaster.DeadEnemies += 1;
   
        //animator.SetBool("IsDead", true);
        GetComponent<Collider2D>().enabled = false;
        this.enabled = false;
    }
}


    

