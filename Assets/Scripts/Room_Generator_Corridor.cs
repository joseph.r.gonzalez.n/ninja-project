﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room_Generator_Corridor : MonoBehaviour
{
    //collect the part to generate
    public GameObject[] roomsParts;

    //generated part
    public GameObject generatedRoom;

    //ControlPosition of the generatedRoom
    public Vector3 offset;


    public GameObject levelGenerator;



    //Variables to check if next room has an exit
    public Transform checkPosition;
    public float Radius;
    public LayerMask rooms;

    //Variables to check if there is already a room next to the exit
    public Transform checkRoom;
    public float distance;


    //bools
    public bool generationStop = false;
    public bool wallbool = false;

    public bool roomCheck;
    void Start()
    {
        levelGenerator = GameObject.FindGameObjectWithTag("LevelGenerator");
        roomCheck = CheckRoom();
    }

    // Update is called once per frame
    void Update()
    {
        if (roomCheck == true)
        {
            generationStop = true;
        }

        if (generationStop == false)
        {
            if (levelGenerator.GetComponent<LevelGeneratorController>().timetoGenerate <= 0)
            {
                GenerateRoom();


                Collider2D[] wall = Physics2D.OverlapCircleAll(checkPosition.position, Radius, rooms);

                if (wall.Length == 0)
                {
                    Debug.Log("Exit");
                    wallbool = false;
                }
                else if (wall.Length >= 1)
                {
                    Debug.Log("Wall");
                    wallbool = true;
                }

            }
        }
        else
        {
            if (wallbool)
            {
                Destroy(generatedRoom);
                generationStop = false;
                wallbool = false;
            }

        }






    }



    void GenerateRoom()
    {
        Debug.Log("Generate");
        int rand = Random.Range(0, roomsParts.Length);
        generatedRoom = Instantiate(roomsParts[rand], transform.position + offset, Quaternion.identity);
        generationStop = true;

    }



    private bool CheckRoom()
    {
        bool isRoom;
        RaycastHit2D RoomInfo = Physics2D.Raycast(checkRoom.position, Vector2.up, distance);
        if (RoomInfo.collider == null)
        {
            Debug.Log("No_Room");
            isRoom = false;
        }
        else
        {
            Debug.Log("Room");
            isRoom = true;
        }
        return isRoom;
    }



}
