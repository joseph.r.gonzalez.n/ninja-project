﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalPlatform : MonoBehaviour
{
    public Transform checkPlayerPos;
    public LayerMask player;
    public float radiusCheck;



    private PlatformEffector2D effector;
    public Joystick joystick;
    public float waitTime;
    public Player_Controller controller;





    void Start()
    {
        effector = GetComponent<PlatformEffector2D>();

        controller = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Controller>();

        if (joystick == null)
        {
            Joystick playerJoystick = GameObject.FindGameObjectWithTag("Joystick").GetComponent<Joystick>();
            joystick = playerJoystick;
            
        }

    }

    // Update is called once per frame
    void Update()
    { 
        //for the joystick
        if(joystick.Vertical < -.5)
        {
            DetectPlayer();
        }

        //Using effector is easily to have plataform that allows the player to go one way and block the other.
        // It is necessary to have a input that will rotate the effector
        //

        //for the keyboard
        if (Input.GetKey(KeyCode.S))
        {
            DetectPlayer();
        }

        if (Input.GetButtonDown("Jump") || controller.touchJump)
        {
            effector.rotationalOffset = 0f;
        }

        //Detects if the player is above the platform
        void DetectPlayer()
        {

            if (Physics2D.OverlapCircle(checkPlayerPos.position, radiusCheck, player))
            {
                effector.rotationalOffset = 180f;
            }


        }

    }

    private void OnDrawGizmosSelected()
    {
        if (checkPlayerPos == null) 
            return;

        Gizmos.DrawWireSphere(checkPlayerPos.position, radiusCheck);
    }


}
