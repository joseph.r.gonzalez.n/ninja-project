﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShurikenAttack : MonoBehaviour
{
    public float speed = 20;
    public int damage = 20;
    public Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = transform.right * speed;
    }

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D enemy)
    {
      

        if (enemy.tag == "Enemy1")
        {
            enemy.GetComponent<Enemy>().TakeDamage(damage);
        }
        else if (enemy.tag == "Enemy2")
        {
            enemy.GetComponent<EnemyRange>().TakeDamage(damage);
        }

        //Debug.Log(collision.name);
        Destroy(gameObject);

    }
}
