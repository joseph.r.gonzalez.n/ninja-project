﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Enemy_Patrol_Testing : MonoBehaviour
{
    Rigidbody2D rb;
    public float speed;
    public float TimetochangeDirection;
    public float setTime;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        TimetochangeDirection = setTime;
    }

    // Update is called once per frame
    void Update()
    {
        ChangeDirection();


        Vector2 velocity = Vector2.left;
        rb.velocity = velocity * speed;
    }

    void ChangeDirection()
    {
        if(TimetochangeDirection <= 0)
        {
            speed = -speed;
            transform.Rotate(0f, 180f, 0f);
            TimetochangeDirection = setTime;
        }
        TimetochangeDirection -= Time.deltaTime;
    }

}
