﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRangeAttack : MonoBehaviour
{


    //square area to check player position
    public Vector3 boxSize;
    public float angle;



    public Transform playerPos;
    public Animator animator;
    public Transform chaseCheck;
    public Collider2D[] playerCheck;
    public LayerMask playerMask;

    //in order to shoot the arrow
    public Transform firePoint;
    public GameObject arrow;



    
    public bool isFlipped = false;


    private void Start()
    {
        if (playerPos == null)
        {
            Transform player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
            playerPos = player;
        }
        


    }



    private void Update()
    {
        AttackCheck();
    }




    public void AttackCheck()
    {


        //playerCheck = Physics2D.OverlapCircleAll(chaseCheck.position, radiusCheck, playerMask);

        playerCheck = Physics2D.OverlapBoxAll(chaseCheck.position, boxSize, angle, playerMask);

        if (playerCheck.Length > 0)
        {
            animator.SetBool("Attack",true);
            LookAtPlayer();
        }
        else
        {
            animator.SetBool("Attack", false);
        }
        



    }


    public void AttackRange()
    {
        //Debug.Log("Attack");
        Instantiate(arrow, firePoint.position, firePoint.rotation);
    }




    public void LookAtPlayer()
    {
        Vector3 flipped = transform.localScale;
        flipped.z *= -1f;

        if (transform.position.x > playerPos.position.x && isFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            isFlipped = false;

        }
        else if (transform.position.x < playerPos.position.x && !isFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            isFlipped = true;
        }


    }



    private void OnDrawGizmosSelected()
    {
        if (chaseCheck == null)
            return;
        Gizmos.DrawWireCube(chaseCheck.position, boxSize);
    }



}
