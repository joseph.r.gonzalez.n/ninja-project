using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CharacterController2D : MonoBehaviour
{

	public ParticleSystem dust;



	[SerializeField] private float m_JumpForce = 400f;                          // Amount of force added when the player jumps.
	[SerializeField] private float m_JumpForcex = 400f;
	[Range(0, 1)] [SerializeField] private float m_CrouchSpeed = .36f;			// Amount of maxSpeed applied to crouching movement. 1 = 100%
	[Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;	// How much to smooth out the movement
	[SerializeField] private bool m_AirControl = false;							// Whether or not a player can steer while jumping;
	[SerializeField] private LayerMask m_WhatIsGround;                          // A mask determining what is ground to the character
	[SerializeField] private LayerMask m_WhatIsWall;                              // A mask determining what is ground to the character
	[SerializeField] private Transform m_GroundCheck;                           // A position marking where to check if the player is grounded.
	[SerializeField] private Transform m_WallCheck;                             // A position marking where to check if the player is on a wall.
	[SerializeField] private Transform m_CeilingCheck;							// A position marking where to check for ceilings
	[SerializeField] private Collider2D m_CrouchDisableCollider;				// A collider that will be disabled when crouching

	const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
	const float k_WallRadius = .5f;
	public bool m_Grounded;
	private bool m_Wall;  // Whether or not the player is grounded.
	const float k_CeilingRadius = .2f; // Radius of the overlap circle to determine if the player can stand up
	private Rigidbody2D m_Rigidbody2D;
	public bool m_FacingRight = true;  // For determining which way the player is currently facing.
	private Vector3 m_Velocity = Vector3.zero;
	private Vector3 m_Dash = Vector3.zero;


	private int extrajumps;
	public int extrajumpsValue;
	public int controlForce;
	public float dashSpeed;


	[Header("Events")]
	[Space]

	public UnityEvent OnLandEvent;

	[System.Serializable]
	public class BoolEvent : UnityEvent<bool> { }

	public BoolEvent OnCrouchEvent;
	private bool m_wasCrouching = false;

	private void Awake()
	{

		extrajumps = 0;

		m_Rigidbody2D = GetComponent<Rigidbody2D>();

		if (OnLandEvent == null)
			OnLandEvent = new UnityEvent();

		if (OnCrouchEvent == null)
			OnCrouchEvent = new BoolEvent();



	}

	private void FixedUpdate()
	{
		bool wasGrounded = m_Grounded;
		bool wasInWall = m_Wall;
		m_Wall = false;
		m_Grounded = false;
		//For the Ground:
		// The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
		// This can be done using layers instead but Sample Assets will not overwrite your project settings.
		Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
		for (int i = 0; i < colliders.Length; i++)
		{
			if (colliders[i].gameObject != gameObject)
			{
				m_Grounded = true;
				GetComponent<Player_Controller>().secondJump = false;
				
				if (!wasGrounded)
					OnLandEvent.Invoke();
			}
		}


		//For the Walls:
		Collider2D[] colliders_wall = Physics2D.OverlapCircleAll(m_WallCheck.position, k_WallRadius, m_WhatIsWall);
		for (int i = 0; i < colliders_wall.Length; i++)
		{
			if (colliders_wall[i].gameObject != gameObject)
			{
				m_Wall = true;
				if (!wasInWall)
					OnLandEvent.Invoke();
			}
		}



	}

	//method to move the player jump dash and crouch
	public void Move(float move, bool crouch, bool jump, bool dash)
	{

		if (dash)
		{
			

			if (!m_FacingRight)
			{
				Vector3 targetdash = new Vector2(m_Rigidbody2D.velocity.x - dashSpeed, m_Rigidbody2D.velocity.y);
				m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetdash, ref m_Dash, 0.05f);


			}
			else if (m_FacingRight)
			{
				Vector3 targetdash2 = new Vector2(m_Rigidbody2D.velocity.x + dashSpeed, m_Rigidbody2D.velocity.y);
				m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetdash2, ref m_Dash, 0.05f);
			}
			
		}
  //      



		// If crouching, check to see if the character can stand up
		if (!crouch)
		{
			// If the character has a ceiling preventing them from standing up, keep them crouching
			if (Physics2D.OverlapCircle(m_CeilingCheck.position, k_CeilingRadius, m_WhatIsGround))
			{
				crouch = true;
			}
		}

		//only control the player if grounded or airControl is turned on
		if (m_Grounded || m_AirControl)
		{

			// If crouching
			if (crouch)
			{
				if (!m_wasCrouching)
				{
					m_wasCrouching = true;
					OnCrouchEvent.Invoke(true);
				}

				// Reduce the speed by the crouchSpeed multiplier
				move *= m_CrouchSpeed;

				// Disable one of the colliders when crouching
				if (m_CrouchDisableCollider != null)
					m_CrouchDisableCollider.enabled = false;
			} else
			{
				// Enable the collider when not crouching
				if (m_CrouchDisableCollider != null)
					m_CrouchDisableCollider.enabled = true;

				if (m_wasCrouching)
				{
					m_wasCrouching = false;
					OnCrouchEvent.Invoke(false);
				}
			}

			// Move the character by finding the target velocity
			Vector3 targetVelocity = new Vector2(move * 10f, m_Rigidbody2D.velocity.y);
			// And then smoothing it out and applying it to the character
			m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);

			// If the input is moving the player right and the player is facing left...
			if (move > 0 && !m_FacingRight)
			{
				// ... flip the player.
				Flip();
			}
			// Otherwise if the input is moving the player left and the player is facing right...
			else if (move < 0 && m_FacingRight)
			{
				// ... flip the player.
				Flip();
			}
		}

		if (!m_Grounded && jump && extrajumps > 0 && !m_Wall)
		{
			//m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce - controlForce));

			m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, 1f * m_JumpForce);
			
			extrajumps--;

		}

		// If the player should jump...
		if (m_Grounded && jump)
		{
			// Add a vertical force to the player.
			//m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
			m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, 1f * m_JumpForce);
			extrajumps = extrajumpsValue;
			m_Grounded = false;
			GetComponent<Player_Controller>().secondJump = true;
		}



		// Jump for the walls

		if (jump)
		{
			if (m_Wall && !m_Grounded)
			{
				if (!m_FacingRight)
				{
					m_Rigidbody2D.velocity = new Vector2(1.4f * m_JumpForce, 1.3f * m_JumpForce);

				}
				else if (m_FacingRight)
				{
					m_Rigidbody2D.velocity = new Vector2(-1.4f * m_JumpForce, 1.3f * m_JumpForce);
				}
			}
			else if (m_Wall && m_Grounded)
			{
				m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, 1f * m_JumpForce);
			}



		}





	}


	


	private void Flip()
	{
		// Switch the way the player is labelled as facing.
		m_FacingRight = !m_FacingRight;

		// Multiply the player's x local scale by -1.
		//Vector3 theScale = transform.localScale;
		//theScale.x *= -1;
		transform.Rotate(0f, 180f, 0f);
		//transform.localScale = theScale;

		

	}


	private void OnDrawGizmosSelected()
	{
		if (m_WallCheck == null)
			return;
		Gizmos.DrawWireSphere(m_WallCheck.position, k_WallRadius);
	}


	public void CreateDust()
	{
		dust.Play();



	}




}
