﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{

    //particle system
    public ParticleSystem trailAttack;

    //animator for the control of the animations
    public Animator animator;

    
    //position for the attack point
    public Transform attackPoint;


    //detect only object that are in this layer
    [SerializeField] private LayerMask destroyObjectLayer;


    //attack behaviour
    public float attackRange = 0.5f;
    public int attackDamage = 40;
    public float attackRate = 2f;
    float nextAttackTime = 0f;


    //For range attack
    public GameObject projectile;
    public bool IsRangeAttack;


    //touch controllers
    public bool touchattack;


    // Update is called once per frame
    void Update()
    {
        if(Time.time >= nextAttackTime)
        {
            //if (Input.GetButtonDown("Attack") || touchattack)
            //{
            //    if (IsRangeAttack)
            //    {
            //        RangeAttack();
            //    }
            //    else
            //    {
            //        MeleeAttack();
            //    }

            //    nextAttackTime = Time.time + 1 / attackRate;
            //    touchattack = false;
            //}

            

            if (Input.GetAxisRaw("Vertical") > 0 && Input.GetButtonDown("Attack"))
            {
                Debug.Log("Up Attack");
                nextAttackTime = Time.time + 1 / attackRate;
            }else if (animator.GetBool("IsJumping") && Input.GetAxisRaw("Vertical") < 0 && Input.GetButtonDown("Attack"))
            {
                Debug.Log("Down Attack");
            }
            else if (Input.GetButtonDown("Attack"))
            {
                Debug.Log("Left-Right Attack");
                MeleeAttack();

            }


        }
        
    }


    //public void StopWhenAttacking()
    //{
    //    GetComponent<Player_Controller>().runSpeed = 70;
    //}


    void MeleeAttack()
    {
        //Stop player
        //GetComponent<Player_Controller>().runSpeed= 0;
        //attack while jumping
        if (GetComponent<Player_Controller>().secondJump)
        {
            animator.SetBool("IsJumping", false);
            animator.SetTrigger("IsRollingAttacking");
        }//attack while in ground
        else
        {
            animator.SetTrigger("IsAttacking");
            //trailAttack.Play();
            //GetComponent<AttackEffects>().SpawnAttackEffect();
        }
        

        //Detect enemies in range of attack
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position,attackRange, destroyObjectLayer);

        // Damage the enemies

        foreach(Collider2D enemy in hitEnemies)
        {
            if(enemy.tag == "Enemy1")
            {
                enemy.GetComponent<Enemy>().TakeDamage(attackDamage);
            }else if(enemy.tag == "Enemy2")
            {
                enemy.GetComponent<EnemyRange>().TakeDamage(attackDamage);
            }else if(enemy.tag == "Item")
            {
                enemy.GetComponent<ItemStats>().TakeDamage(attackDamage);
                GetComponent<AttackEffects>().CreateHitEffect(attackPoint.position);
            }

            //Debug.Log("We hit" + enemy.name);
        }

    }

    void RangeAttack()
    {
        Instantiate(projectile, attackPoint.position,attackPoint.rotation);
    }




    private void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
            return;
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }


}
