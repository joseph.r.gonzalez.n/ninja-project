﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smasher_Controller : MonoBehaviour
{
    //Effects
    public ParticleSystem hitDust;


    //Desired position to hit
    public Transform hitPoint;
    //Original position of the smasher
    Vector3 initialPos;

    [Header("Speed Parameters")]
    //speed to hit
    public float hitSpeed;
    //speed to recharge
    public float rechargeSpeed;

    [Header("Flags")]
    //conditions to perform the hit
    public bool timetoHit;
    public bool hitOccur;
    [Header("Counter")]
    public float startHittingSet;


    void Start()
    {
        initialPos = transform.position;

    }

    // Update is called once per frame
    void Update()
    {
        //Start to move when starthittingset is 
        StartMoving();
      

        if (timetoHit)
        {
            if (!hitOccur)
            {
                HitPhase();
                
            }
            else
            {
                RechargePhase();
                //Debug.Log("NoHit");
            }
        }
    }

    //Start the movement of the smasher
    void StartMoving()
    {
        if (!timetoHit)
        {
            if (startHittingSet <= 0)
            {
                timetoHit = true;
            }

            startHittingSet -= Time.deltaTime;

        }
    }

//Controls the velocity for the hitting phase
    void HitPhase()
    {
        transform.position = Vector2.MoveTowards(transform.position, hitPoint.position,hitSpeed * Time.deltaTime);
        if(transform.position == hitPoint.position)
        {
            hitOccur = true;
            hitDust.Play();
        }
    }

    //Controls the velocity for the recharge phase
    void RechargePhase()
    {
        transform.position = Vector2.MoveTowards(transform.position, initialPos, rechargeSpeed * Time.deltaTime);
        if(transform.position == initialPos)
        {
            hitOccur = false;

        }

    }


}
