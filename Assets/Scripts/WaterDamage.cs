﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterDamage : MonoBehaviour
{
    public float timeToDamage;
    public BoxCollider2D waterLevel;
    public Animator floorAnimator;

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            if(timeToDamage <= 0)
            {
                collision.gameObject.GetComponent<PlayerHealth>().TakeDamage(10);
                timeToDamage = 0.5f;
                Debug.Log("Hit");
            }

            timeToDamage -= Time.deltaTime;    
        }


    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        waterLevel.enabled = true;
        floorAnimator.SetBool("PlayerFall", false);
    }
}
