﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour
{
    public static GameMaster gm;
    public Transform playerPrefab;
    public Transform spawnPoint;

    public GameObject endpanel;

    public int spawnDelay = 2;
    public static int DeadEnemiesUI = 0;
    public int DeadEnemies = 0;



    void Start()
    {
        //GameObject.FindGameObjectWithTag("EndGamePanel").SetActive(false);

        if (gm == null) {
            gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();
        }
    }

    private void Update()
    {
        DeadEnemiesUI = DeadEnemies;
        


    }





    public IEnumerator RespawnPlayer() {
        Debug.Log("TODO:Add waiting for spawn sound");
        yield return new WaitForSeconds(spawnDelay);

        Instantiate(playerPrefab, spawnPoint.position, spawnPoint.rotation);
        Debug.Log("Add Spawn Particles");
    }
    public void KillPlayer() {
        Destroy(GameObject.FindGameObjectWithTag("Player").gameObject);
        gm.StartCoroutine(gm.RespawnPlayer());
    }

    public void EndGame()
    {
        //Time.timeScale = 0;
        Time.timeScale = 0;
        endpanel.SetActive(true);



    }

    public void Restart ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Time.timeScale = 1;
    }





}
