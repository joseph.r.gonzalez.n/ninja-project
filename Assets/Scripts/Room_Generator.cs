﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Room_Generator : MonoBehaviour
{
    //collect the part to generate
    public GameObject[] roomsParts;
    public GameObject[] endParts;

    //generated part
    public GameObject generatedRoom;

   //ControlPosition of the generatedRoom
    public Vector3 offset;
    

    public GameObject levelGenerator;

    

    //Variables to check if next room has an exit
    public Transform checkPosition;
    public float Radius;
    public LayerMask rooms;

    //Variables to check if there is already a room next to the exit
    public Transform checkRoom;
    public Vector2 checkDirection;
    public float distance;


    //bools
    public bool generationStop = false;
    public bool wallbool =false;
    public bool roomCheck;
    //int of wrong rooms
    public int deletedRoom;
    void Start()
    {
        endParts = Resources.LoadAll<GameObject>("EndPieces");
        levelGenerator = GameObject.FindGameObjectWithTag("LevelGenerator");
        
    }

    // Update is called once per frame
    void Update()
    {
        roomCheck = CheckRoom();

        if (generationStop == false)
            {
                if (levelGenerator.GetComponent<LevelGeneratorController>().timetoGenerate <= 0)
                {
                    GenerateRoom();


                    Collider2D[] wall = Physics2D.OverlapCircleAll(checkPosition.position, Radius, rooms);

                    if (wall.Length == 0)
                    {
                        //Debug.Log("Exit");
                        wallbool = false;
                    }
                    else if (wall.Length >= 1)
                    {
                        //Debug.Log("Wall");
                        wallbool = true;
                    }

                }
            

            if (roomCheck)
            {
                generationStop = true;
            }


        }
        else
        {
            if (wallbool)
            {
                deletedRoom += 1;
                if(deletedRoom <= 50)
                {
                    Destroy(generatedRoom);
                    generationStop = false;
                    wallbool = false;
                }
                else
                {
                    Destroy(generatedRoom);
                    GenerateEnding();
                    wallbool = false;
                }
                
            }


        }

        if (levelGenerator.GetComponent<LevelGeneratorController>().endofLevel && roomCheck == false && generationStop == false)
        {
            GenerateEnding();
            generationStop = true;
            roomCheck = true;


        }

        


    }
    


    void GenerateRoom()
    {
        //Debug.Log("Generate");
        int rand = UnityEngine.Random.Range(0, roomsParts.Length);
        GameObject newRoom = roomsParts[rand];
        if (this.gameObject.GetInstanceID() != newRoom.GetInstanceID())
        {
            generatedRoom = Instantiate(newRoom, transform.position + offset, Quaternion.identity);
            generationStop = true;
        }

       
        

    }




    void GenerateEnding()
    {
        Debug.Log("Ending");
        Instantiate(endParts[0], transform.position + offset, Quaternion.identity);

    }


    private bool CheckRoom()
    {
        bool isRoom;
      
        RaycastHit2D RoomInfo = Physics2D.Raycast(checkRoom.position, checkDirection, distance,rooms);
        if (RoomInfo.collider == null)
        {
            //Debug.Log("No_Room");
            isRoom = false;
        }
        else
        {
            //Debug.Log("Room");
            isRoom = true;
        }
        return isRoom;
    }













    private void OnDrawGizmosSelected()
    {
        if (checkPosition == null)
            return;
        Gizmos.DrawWireSphere(checkPosition.position, Radius);
    }


   
}
